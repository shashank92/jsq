function Queue() {
    var argsLength = arguments.length, 
        input, push, i;
    if (typeof this === 'undefined' ||
            this === Function('return this')()) {
        return Queue.from(arguments);
    }
    this.input = [];
    this.output = [];
    if (argsLength) {
        input = this.input
        push = input.push.bind(input);
        for (i = 0; i < argsLength; i++) {
            push(arguments[i]);
        }
    }
}

Queue.from = function (arrayLike) {
    var i = 0,
        l = arrayLike.length,
        q = new Queue(),
        enqueue = q.enqueue.bind(q);
    for (; i < l; i++) {
        enqueue(arrayLike[i]);
    }
    return q;
}

Queue.prototype = {
    enqueue: function (x) {
        this.input.push(x);
        return this;
    },
    dequeue: function () {
        var output = this.output;
        if (!output.length) {
            output = this.output = this.input.reverse();
            this.input = [];
        }
        return output.pop();
    },
    toArray: function () {
        return this.output.slice().reverse().concat(this.input);
    },
    toString: function () {
        return this.toArray().toString();
    }
};

/* Tests
var q1 = new Queue().enqueue(4).enqueue(5).enqueue(6),
    q2 = Queue().enqueue(4).enqueue(5).enqueue(6),
    q3 = new Queue(4, 5, 6),
    q4 = Queue(4, 5, 6);
console.log(q1.toArray());
console.log(q1.toString());
console.log(q2.toArray());
console.log(q2.toString());
console.log(q3.toArray());
console.log(q3.toString());
console.log(q4.toArray());
console.log(q4.toString());
console.log(q1.dequeue());
console.log(q1);
//*/
